# This is the page where we will store all the page objects
require 'page-object'
require 'page-object/page_factory'

class Page_Objects_Page
  include PageObject

	#Homepage Elements
	  	
		link(:cat_women,:link_text => "Women")
		link(:geo_popup, :id => 'geodetection-stay-link')
		#span(:sub_cat_tops, (:css, ('data-gtag' = "women|Tops"))
		link(:sub_cat_tops, :css => 'ul.row:nth-child(13) > ul:nth-child(2) > li:nth-child(6) > span:nth-child(2) > a:nth-child(1)')
		link(:sub_cat_women_tops, :class => 'last-category')
		
		
	#End of Homepage elements



end