require 'page-object'
#require 'byebug'
class Cotton_On_homepage
  include PageObject
  page_url "#{FigNewton.base_url}"
  
=begin 
def check_url
		if (@browser.url == page_url)
		puts "Web site URL is correct"
		end
end
=end
def check_page_load
	@ready= @browser.ready_state.eql? "complete"
	if (@ready ==true)
		sleep(5)
		puts "Page is loaded"		
	end
end


def click_stay_current_site
	@@geo_pop_up = Page_Objects_Page.new(@browser)
	Watir::Wait.until {@@geo_pop_up.geo_popup_element}.click
	sleep 5
end	

def click_women_cat
	#@browser.a(:'data-gtag',"Women").click
	@@women_category = Page_Objects_Page.new(@browser)
	Watir::Wait.until {@@women_category.cat_women_element}.when_present.click
	sleep 5
end


def click_tops_subcat
	@@tops_sub_category = Page_Objects_Page.new(@browser)
	Watir::Wait.until {@@tops_sub_category.sub_cat_tops_element}.click
	@current_url = @browser.url
	p @current_url
end

def assert_tops_women_sub
	@@tops_sub_cat = Page_Objects_Page.new(@browser)
	@@sub_cat_page_title = @@tops_sub_cat.sub_cat_women_tops_element.attribute('title')
	if (Watir::Wait.until {(@@sub_cat_page_title).include?("Go to Tops")}.should==true)
			p @@sub_cat_page_title
			puts "Title of sub category page is validated"
		else
			puts "Title of sub category page is NOT validated"
	end
	@@sub_cat_page_text = @@tops_sub_cat.sub_cat_women_tops_element.text
	if (Watir::Wait.until {(@@sub_cat_page_text).include?("Tops")}.should==true)
			p @@sub_cat_page_text
			puts "Text of sub category page is validated"
		else
			puts "Text of sub category page is NOT validated"
	end		
end

end

