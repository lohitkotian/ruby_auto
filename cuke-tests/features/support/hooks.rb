require 'rubygems'
require 'page-object'
require 'watir'
require 'page-object/page_factory'
require 'log4r'
require 'cucumber'
require 'selenium-webdriver'
require 'watir-scroll'



Before do|scenario|

  case 

    when ENV['LOCAL']
      begin
        ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
        @browser = Watir::Browser.new :chrome 
        @browser.window.maximize
        @browser.driver.manage.timeouts.implicit_wait = 300
        @browser.cookies.clear
        @browser.driver.manage.window.maximize
        #PageObject.default_element_wait=(10)
      end
  
  else

          ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
          @browser = Watir::Browser.new :chrome 
          @browser.window.maximize
          @browser.driver.manage.timeouts.implicit_wait = 300
          @browser.cookies.clear
          @browser.driver.manage.window.maximize
  end



end #before scenario




# "after all"
After do |scenario| 

 #@browser.close

     #if (scenario.passed?)
      
      #puts 'Scenario Passed !'

     #else

      #puts 'Scenario Failed !'
      

    #end

end



AfterStep('@screen') do
   filename = DateTime.now.strftime("%d%b%Y%H%M%S")

     @browser.screenshot.save ("C:/Users/#{filename}.png")
end


Around('@multipletimes') do |scenario, block|
  $counter = 0
  $total_times_to_run = 3
  while $counter < $total_times_to_run  do
     block.call
     puts("Total times scripts were repeated = #$counter" )
     $counter +=1
  end
end
