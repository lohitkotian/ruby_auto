Given(/^I am on the Cotton On Home Page$/) do
	visit(Cotton_On_homepage)
end

And (/^I check the geolocation popup$/) do
	on(Cotton_On_homepage).click_stay_current_site
end

When(/^I click on the Women category in the menu$/) do
	on(Cotton_On_homepage).click_women_cat
end

And (/^I click on the Tops Women subcategory$/) do
	on(Cotton_On_homepage).click_tops_subcat
end

Then(/^The Tops Women subcategory page is loaded$/) do
  	on(Cotton_On_homepage).assert_tops_women_sub
end


