Feature: Cotton On HomePage Sanity Test Case Scenarios
     As a tester I want to check whether the sanity test cases for Cotton On Homepage work as expected
   
@sanity
Scenario: As a user I am able to go to the Cotton On Homepage and browse around
  Given I am on the Cotton On Home Page
  And I check the geolocation popup 
  When I click on the Women category in the menu
  And I click on the Tops Women subcategory
  Then The Tops Women subcategory page is loaded


