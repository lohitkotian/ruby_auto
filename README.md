# Web Automation Framework using Ruby

Uses Cucumber- Selenium- Watir Framework

Uses Pageobject model

Most of the test cases are built with Test Driven Development format

Ruby 2.4.1 and above 
selenium-webdriver 3.4.4 and above
cucumber 2.4.0 and above
saucelabs 0.5 (only required to configure saucelabs for cross browser/ multiple OS testing) and above
Watir 6.4.2 and above
